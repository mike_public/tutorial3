/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mike.ejemplo;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import java.io.File;
import java.util.List;
import libs.Archivos;
import libs.MyListArgs;
import libs.MySintaxis;

/**
 *
 * @author tonsq
 */
public class Componente {
    String IN_DATA;
    String OUT;
    
    Componente(String[]args){
        String ConfigFile;
        MyListArgs Param;
        Param      = new MyListArgs(args);
        ConfigFile = Param.ValueArgsAsString("-CONFIG", "");
        if (!ConfigFile.equals(""))
            Param.AddArgsFromFile(ConfigFile);

        String    Sintaxis = "-IN_DATA:str -OUT:str";
        MySintaxis  Review = new MySintaxis(Sintaxis, Param);
        
        IN_DATA            = Param.ValueArgsAsString("-IN_DATA", "");
        OUT                = Param.ValueArgsAsString("-OUT"    , "");
    }
    public void doSomethingCool(){
        String content = "";
        String files[] = new File(this.IN_DATA).list();
        new File(this.OUT).mkdirs();
        for (int i = 0; i < files.length; i++) {
            String file = files[i];
            content = getContent(this.IN_DATA+File.separator+file);
            content = count_words_in_string_guava(content);
            Archivos.escribeArchivo(this.OUT+File.separator+file, content);
        }
    }
    public String count_words_in_string_guava(String phrase){
        String out = "";
        List<String> list = Splitter.on(CharMatcher.WHITESPACE)
            .splitToList(phrase);
        Multiset<String> items = HashMultiset.create(list);
        
        for (Multiset.Entry<String> entry : items.entrySet()) {
            out += entry.getElement() + " - " + entry.getCount() + " times\n";
        }
        return out;
    }
    public String getContent(String file){
        return Archivos.leerArchivoTexto(file);
    }
    
}
