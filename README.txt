Componente que cambia el texto de archivo a mayúsculas

Sintaxis:
-IN_DATA   Directorio que contiene los archivos de texto a procesar
-OUT       Directorio en donde se va a almacenar el resultado del componente con los datos de entrada encontrado en -IN_DATA

Tests:
En el primer test se prueba el componente con 3 archivos en español con codificación UTF-8
En el segundo test se prueba el componente con 2 archivos en español con codificacion UTF-8
En el tercer test se prueba el componente con 1 archivo en idioma ruso con codificacion UTF-8

Autor: Miguel Ángel García Calderón
mgcalderon@outlook.es
  